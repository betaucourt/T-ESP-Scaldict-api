const { ClientRequest } = require('http');
const { Stream } = require('stream');
const { cliu } = require('winston/lib/winston/config');
const { Worker, parentPort } = require('worker_threads');
const User = require('./user');
//const EventServer = require('http').createServer();
const SocketHandler = require('./socketHandler');
const { handle } = require("express/lib/router");
const net = require('net');
const { list } = require("pm2");
/*const EventIo = require("socket.io")(EventServer, {
  cors: {
    origin: "*",
    methods: ["*"]
  }
});*/



var socketHandler = new SocketHandler();
var actualPort = 8000;
/*EventIo.on('connection', client => {
  console.log('client connected on EventSocket');
  client.send('Connected on EventSocket');
  client.on('disconnection', message => {
    console.log('client disconnected on EventSocket');
    client.send('disconnected');
  })
});*/
//EventServer.listen(8080);

const StreamServer = require('http').createServer();
const StreamIo = require("socket.io")(StreamServer, {
  cors: {
    origin: "*",
    methods: ["*"]
  }
});

StreamIo.on('connection', client => {
  console.log('client connected on streamSocket');

  client.send('Connected On StreamSocket');
  socketHandler.addUser(new User(client.id, client.handshake.address, client));

  client.on('config', message => {
    try {
      socketHandler.setConfig(client.id, JSON.parse(message));
    }catch(e) {};
  })

  client.on('disconnection', message => {
    if (message === "testSocket") {
    } else {
      socketHandler.removeUser(client);
      setTimeout(() => client.disconnect(true), 5000);
      if (socketHandler.users.length === 0) {
        console.log("disconnection");
        StreamServer.close();
        EventServer.close();
      }
    }
    console.log('client disconnected on streamSocket');

    })

    client.on('partialTranscriptReceived', message => {
    console.log('partial received');
    socketHandler.addMessageToUser(client, message);
    console.log(message);
    client.send("message", "un bout de phrase est arrivé");
  })


  client.on('finalTranscriptReceived', message => {
    console.log('final received');
    console.log(message);
    client.send('message', 'Une phrase est terminée');
    socketHandler.sendToUsers(message, client);
  })

  client.on("try", message => {
    console.log(message)
  });

  client.on('test', message => {
    StreamIo.emit('message', 'Kirikou est petit, mais il est gentil');
  })
});

StreamServer.on('error', function(e) {
  if (e.message.includes("EADDRINUSE")) {
    StreamServer.close();
    actualPort++;
    StreamServer.listen(actualPort);
  }
})

StreamServer.listen(actualPort);

parentPort.on('message', function (message) {
  console.log('receving msg :', message);
});



