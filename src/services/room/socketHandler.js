
const translate = require('@vitalets/google-translate-api');

module.exports = class socketHandler {
  constructor() {
    this.users = [];
  }

  addUser(user) {
    this.users.push(user);
    console.log("User addr " + user.addr + " added");
    return this;
  }

  removeUser(user) {
    this.users.splice(this.users.indexOf(element => { element === user}), 1);
    console.log("User addr " + user.addr + " removed")
  }

  setConfig(userId, config) {
    this.users.find((element) => element.id === userId).lang = config.lang;
  }

  findUser(client) {
    return this.users.find((element) => element.id === client.id)
  }

  async sendToUsers(message, client) {
    this.addMessageToUser(client, message)
    for(var i = 0; this.users.length !== i; i++) {
      await translate(this.getMessage(client), {
          from: this.findUser(client).lang,
          to: this.users[i].lang
      }).then(
      (res) => {
         this.sendMessageTranslated(client, this.users[i].id, res.text);
        }).catch();
    }
    this.removeMessage(client);
  }
  addMessageToUser(client, message) {
    this.users.find((element) => element.id === client.id).message += message;
  }

  removeMessage(client) {
    return (this.users.find((element) => element.id === client.id).message = null);
  }

  getMessage(client) {
    return this.users.find((element) => element.id === client.id).message
  }

  sendMessageTranslated(client, id, message) {
    client.broadcast.to(id).emit('toDisplay', message);

  }

   sendMessageAsync(client, id) {
    client.broadcast.to(id).emit('toDisplay', this.getMessage(client));
  }

}
