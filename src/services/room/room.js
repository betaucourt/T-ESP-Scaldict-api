const User = require('./user');
const { Worker } = require('worker_threads');

module.exports = class Room {
  constructor(users, id) {
    this.users = new Array();
    for (let user of users) {
      this.users.push(this.newUser(user.name, user.addr));
    }
    this.id = id;
    this.worker = new Worker('./src/services/room/SocketWorker.js', {});
  }

  newUser(name, addr) {
    return new User(name, addr);
  }

  insertUser(name, addr) {
    this.users.push(this.newUser(name, addr));
  }

  sendWorkerMessage(message) {
    console.log('sendingMessage');
    this.worker.postMessage(message);
  }

  terminateWorker() {
    this.worker.terminate();
  }

  getUser(name, addr) {
    if (name != null) {
      return this.users.find((element) => element.name == name);
    }
    if (addr != null) {
      return this.users.find((element) => element.addr == addr);
    }
  }
};
