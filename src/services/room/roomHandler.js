const Room = require('./room');
const User = require('./user');

module.exports = class roomHandler {
  constructor() {
    this.rooms = new Array();
    this.size = 0;
  }

  setSize() {
    this.size = rooms.length;
  }
  getSize() {
    let toReturn = this.size;
    return toReturn;
  }

  generateId() {
    let id = this.size;
    if (id < 0) id = 0;
    this.size++;
    return id;
  }

  searchRoom(id) {
    return this.rooms[id];
  }

  getRoom(id) {
    let room = this.searchRoom(id);
    return room;
  }

  sendMessageToRoom(message, id) {
    let room = this.getRoom(id);
    if (!room) return;
    room.sendWorkerMessage(message);
  }

  generateRoom(users) {
    let room = new Room(users, this.generateId()); //new room(users, this.generateId())
    console.log(room.id);
    this.rooms.push(room);
    return room;
  }
};
