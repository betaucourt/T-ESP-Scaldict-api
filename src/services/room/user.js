module.exports = class User {
  constructor(id, addr, lang, client) {
    this.id = id;
    this.addr = addr;
    this.lang = lang;
    this.message;
    this.client = client;
  }

  sendMessage(message) {
    this.client.emit("toDisplay", message);
  }
};
