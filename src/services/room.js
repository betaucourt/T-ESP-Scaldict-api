const generateRoom = function (req, res, next) {
  var room = global.roomHandler.generateRoom(req.body.Users);
  global.roomHandler.sendMessageToRoom('kiki', 0);
  global.roomHandler.sendMessageToRoom('koko', 1);
  global.roomHandler.sendMessageToRoom('tata', 0);
  global.roomHandler.sendMessageToRoom('tete', 1);
  next();
};

module.exports = {
  generateRoom,
};
