const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { authService, userService, tokenService, emailService } = require('../services');

const test = catchAsync(async (req, res) => {
  let test = {
    "EventSocket": ":8080",
    "StreamSocket": ":8081"
  };

  res.status(200);
  res.send(test);
});



module.exports = {
  test,
};
