const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const socketController = require('../../controllers/socket.controller');
const room = require('../../services/room');

const router = express.Router();

router.route('/test').post(room.generateRoom, socketController.test);

module.exports = router;
